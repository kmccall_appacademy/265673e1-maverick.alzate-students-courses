

class Student
  @first_name
  @last_name
  @courses

  attr_accessor :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    # avoid re-enrolling
    return if @courses.include?(course)
    # raise error: course conflict
    raise if @courses.any? { |other| course.conflicts_with?(other) }
    # add course to courses
    @courses.push(course)
    # add student to course's student list
    course.students.push(self) if !course.students.include?(self)
  end

  def course_load
    credit_count = Hash.new(0)

    @courses.each do |course|
      credit_count[ course.department ] += course.credits
    end

    credit_count
  end

end
